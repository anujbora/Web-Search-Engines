package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Use this template to implement the phrase ranker for HW1.
 * 
 * @author congyu
 * @author fdiaz
 */
public class RankerPhrase extends Ranker {

  public RankerPhrase(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
    Vector<ScoredDocument> all = new Vector<ScoredDocument>();

    for (int i = 0; i < _indexer.numDocs(); i++) {
      all.add(scoreDocument(query, i));
    }

    Collections.sort(all, Collections.reverseOrder());
    
    // Normalization
    double maxScore = all.get(0).get_score();
    double minScore = all.get(all.size()-1).get_score();
    double scoreRange = maxScore - minScore;
    
    // If no documents match the search term, use Numviews Ranker
 		if(maxScore == 0 && minScore == 0) {
 			RankerNumviews numviews = new RankerNumviews(_options, _arguments, _indexer);
 			return numviews.runQuery(query, numResults);
 		}
    
    Vector<ScoredDocument> results = new Vector<ScoredDocument>();
    
    for (int i = 0; i < Math.min(all.size(), numResults); i++) {
      double score = (all.get(i).get_score() - minScore ) / scoreRange;
      all.get(i).set_score(score);
      results.add(all.get(i));
    }
    /////
    
    return results;
  }
  
  private ScoredDocument scoreDocument(Query query, int did) {
    DocumentFull doc = (DocumentFull) _indexer.getDoc(did);
    double score = 0;
    
    Vector<String> allBodyTokens = doc.getConvertedBodyTokens();
    Vector<String> allQueryTokens = query._tokens;
    
    if (allQueryTokens.size() == 1) {
      // get frequency of the term in the document
    	String q = allQueryTokens.get(0);
      for (String t : allBodyTokens) {
        if (t.equals(q)) {
          score = score + 1.0;
        }
      }
    } else {
      for (int i = 0; i < (allQueryTokens.size() - 1); i++) {
        for (int j = 0; j < (allBodyTokens.size() - 1); j++) {
          if (allQueryTokens.get(i).equals(allBodyTokens.get(j)) 
          		&& allQueryTokens.get(i + 1).equals(allBodyTokens.get(j + 1))) {
            score = score + 1.0;
          }
        }
      }
    }
    return new ScoredDocument(query._query, doc, score);
  }
}