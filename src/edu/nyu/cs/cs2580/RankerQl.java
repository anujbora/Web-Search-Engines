package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Use this template to implement the query likelihood ranker for HW1.
 * 
 * @author congyu
 * @author fdiaz
 */
public class RankerQl extends Ranker {
	
  private double lambda = 0.5;

  public RankerQl(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
	    Vector<ScoredDocument> all = new Vector<ScoredDocument>();
		
	    HashMap<String,Double> query_probab = new HashMap<String,Double>();
	    
	    long N = _indexer._totalTermFrequency;
	    
	    for(String token: query._tokens) {
	    	
	    	if(!query_probab.containsKey(token)) {
	    		
	    		double value = _indexer.corpusTermFrequency(token);
	    		value = lambda * (value / N);
	    		
	    		query_probab.put(token, value);
	    		
	    	}
	    }
	    
	    
		for (int i = 0; i < _indexer.numDocs(); ++i) {
	      all.add(scoreDocument(query, query_probab, i));
	    }
		    
		Collections.sort(all, Collections.reverseOrder());
		
		// Normalizing Scores
		double maxScore = all.get(0).get_score();
		double minScore = all.get(all.size()-1).get_score();
		double scoreRange = maxScore - minScore;
		
		// If no documents match the search term, use Numviews Ranker
		if(maxScore == 0 && minScore == 0) {
			RankerNumviews numviews = new RankerNumviews(_options, _arguments, _indexer);
			return numviews.runQuery(query, numResults);
		}

		Vector<ScoredDocument> results = new Vector<ScoredDocument>();
		for (int i = 0; i < Math.min(all.size(), numResults); i++) {
		  double score = (all.get(i).get_score() - minScore ) / scoreRange;
		  all.get(i).set_score(score);
		  results.add(all.get(i));
		}
		    
		return results;
	  }
	  
	  private ScoredDocument scoreDocument(Query query, HashMap<String,Double> query_probab, int did) {
			
		    DocumentFull doc = (DocumentFull) _indexer.getDoc(did);
		    
		    double score = 0;
		    
		    Vector<String> allTokens = doc.getConvertedBodyTokens();
		    
		    int D = allTokens.size();
		    
		    HashMap<String,Integer> tf = new HashMap<String,Integer>();
		    
		    for(String token: allTokens) {
		    	
		    	if(!tf.containsKey(token)) {
		    		
		    		tf.put(token, 1);
		    	} else
		    	{
		    		tf.replace(token, tf.get(token) + 1);
		    	}
		    }
		    
		    for(String token: query._tokens) {
		    	
		    	double value = 0;
		    	
		    	if(tf.containsKey(token)) {
		    		value = tf.get(token);
		    	}
		    	
		    	value = value / D;
		    	value = (1 - lambda) * value;
		    	value += query_probab.get(token);
		    	if(value != 0)
		    		value = Math.log10(value);
		    	score += value;
		    	
		    }
		    
		    return new ScoredDocument(query._query, doc, score);
	  }
	}
