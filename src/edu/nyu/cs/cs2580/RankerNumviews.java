package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Use this template to implement the numviews ranker for HW1.
 * 
 * @author congyu
 * @author fdiaz
 */
public class RankerNumviews extends Ranker {

  public RankerNumviews(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
    Vector<ScoredDocument> all = new Vector<ScoredDocument>();
    
    for (int i = 0; i < _indexer.numDocs(); ++i) {
	      all.add(scoreDocument(query, i));
	}
    
    Collections.sort(all, Collections.reverseOrder());
    
    // Normalizing Scores
    double maxScore = all.get(0).get_score();
    double minScore = all.get(all.size()-1).get_score();
    double scoreRange = maxScore - minScore;
 	if(maxScore == 0 && minScore == 0){
 	   all.setSize(numResults);
 	   return all;	
 	}

 	Vector<ScoredDocument> results = new Vector<ScoredDocument>();
 	
 	for (int i = 0; i < Math.min(all.size(), numResults); i++) {
 	   double score = (all.get(i).get_score() - minScore ) / scoreRange;
 	   all.get(i).set_score(score);
 	   results.add(all.get(i));
 	}
 		    
 	return results;
     
  }
   
  private ScoredDocument scoreDocument(Query query, int did) {
	
    Document doc = _indexer.getDoc(did);
    double score = doc.getNumViews();
    	 
    return new ScoredDocument(query._query, doc, score);
  }
}


