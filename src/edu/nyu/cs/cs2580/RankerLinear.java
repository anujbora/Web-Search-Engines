package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Use this template to implement the linear ranker for HW1. You must
 * use the provided _betaXYZ for combining the signals.
 * 
 * @author congyu
 * @author fdiaz
 */
public class RankerLinear extends Ranker {
  private float _betaCosine = 1.0f;
  private float _betaQl = 1.0f;
  private float _betaPhrase = 1.0f;
  private float _betaNumviews = 1.0f;

  public RankerLinear(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
    _betaCosine = options._betaValues.get("beta_cosine");
    _betaQl = options._betaValues.get("beta_ql");
    _betaPhrase = options._betaValues.get("beta_phrase");
    _betaNumviews = options._betaValues.get("beta_numviews");
  }
  
  static class DocumentIDComparator implements Comparator<ScoredDocument>
  {
      public int compare(ScoredDocument d1, ScoredDocument d2)
      {
          return d1.get_doc_id().compareTo(d2.get_doc_id());
      }
  }
  
  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
    System.out.println("  with beta values" +
        ": cosine=" + Float.toString(_betaCosine) +
        ", ql=" + Float.toString(_betaQl) +
        ", phrase=" + Float.toString(_betaPhrase) +
        ", numviews=" + Float.toString(_betaNumviews));
    Vector<ScoredDocument> all = new Vector<ScoredDocument>();

    DocumentIDComparator comparator = new DocumentIDComparator();
    
    // Obtain all the documents scores by using Cosine Ranker
    RankerCosine cosine = new RankerCosine(_options, _arguments, _indexer);
    Vector<ScoredDocument> cosineResults = cosine.runQuery(query, _indexer.numDocs());
    Collections.sort(cosineResults, comparator);
    
    // Obtain all the documents scores by using Numviews Ranker
    RankerNumviews numviews = new RankerNumviews(_options, _arguments, _indexer);
    Vector<ScoredDocument> numviewsResults = numviews.runQuery(query, _indexer.numDocs());
    Collections.sort(numviewsResults, comparator);
    
    // Obtain all the documents scores by using Phrase Ranker
    RankerPhrase phrase = new RankerPhrase(_options, _arguments, _indexer);
    Vector<ScoredDocument> phraseResults = phrase.runQuery(query, _indexer.numDocs());
    Collections.sort(phraseResults, comparator);
    
    // Obtain all the documents scores by using QL Ranker
    RankerQl ql = new RankerQl(_options, _arguments, _indexer);
    Vector<ScoredDocument> qlResults = ql.runQuery(query, _indexer.numDocs());
    Collections.sort(qlResults, comparator);
    
    for (int i = 0; i < _indexer.numDocs(); ++i) {
      double score = (_betaCosine * (cosineResults.get(i).get_score())) + 
      		(_betaQl * (qlResults.get(i).get_score())) + 
      				(_betaPhrase * (phraseResults.get(i).get_score())) +
      						(_betaNumviews * (numviewsResults.get(i).get_score()));
      all.add(new ScoredDocument(query._query, _indexer.getDoc(i), score));
    }
    
    Collections.sort(all, Collections.reverseOrder());
    
    // Normalizing Scores
   	double maxScore = all.get(0).get_score();
   	double minScore = all.get(all.size()-1).get_score();
   	double scoreRange = maxScore - minScore;
   	
   	Vector<ScoredDocument> results = new Vector<ScoredDocument>();
   	for (int i = 0; i < Math.min(all.size(), numResults); i++) {
   	  double score = (all.get(i).get_score() - minScore ) / scoreRange;
   	  all.get(i).set_score(score);
   	  results.add(all.get(i));
   	}
    
    return results;
  }
  
}
