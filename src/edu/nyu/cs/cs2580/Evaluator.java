package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Evaluator for HW1.
 * 
 * @author fdiaz
 * @author congyu
 */
class Evaluator {
  public static class DocumentRelevances {
    private Map<Integer, Double> relevances = new HashMap<Integer, Double>();
    private Map<Integer, Double> relevancesWeight = new HashMap<Integer, Double>();
    private int totalRelevantDocsCount = 0;
    
    public DocumentRelevances() { }
    
    public int getTotalRelevantDocsCount() {
      return totalRelevantDocsCount;
    }
    
    public void addDocument(int docid, String grade) {
      double relevance = convertToBinaryRelevance(grade);
      double relevanceWeight = convertToIntegerRelevance(grade);
      if (relevance == 1.0) {
        totalRelevantDocsCount++;
      }
      relevances.put(docid, relevance);
      relevancesWeight.put(docid, relevanceWeight);
    }
    
    public boolean hasRelevanceForDoc(int docid) {
      return relevances.containsKey(docid);
    }
    
    public double getRelevanceForDoc(int docid) {
      return relevances.get(docid);
    }
    
    public double getRelevanceWeightForDoc(int docid) {
      return relevancesWeight.get(docid);
    }
    
    private static double convertToBinaryRelevance(String grade) {
      if (grade.equalsIgnoreCase("Perfect") ||
          grade.equalsIgnoreCase("Excellent") ||
          grade.equalsIgnoreCase("Good")) {
        return 1.0;
      }
      return 0.0;
    }
    
    private static double convertToIntegerRelevance(String grade) {
      if (grade.equalsIgnoreCase("Perfect")) {
        return 10;
      }
      if (grade.equalsIgnoreCase("Excellent")) {
        return 7;
      }
      if (grade.equalsIgnoreCase("Good")) {
        return 5;
      }
      if (grade.equalsIgnoreCase("Fair")) {
        return 1;
      }
      if (grade.equalsIgnoreCase("Bad")) {
        return 0;
      }
      System.out.println("Specified wrong relevance string!");
      return -1;
    }
  }
  
  /**
   * Usage: java -cp src edu.nyu.cs.cs2580.Evaluator [labels] [metric_id]
   */
  public static void main(String[] args) throws IOException {
    Map<String, DocumentRelevances> judgments =
        new HashMap<String, DocumentRelevances>();
    SearchEngine.Check(args.length == 2, "Must provide labels and metric_id!");
    readRelevanceJudgments(args[0], judgments);
    evaluateStdin(Integer.parseInt(args[1]), judgments);
  }

  public static void readRelevanceJudgments(
      String judgeFile, Map<String, DocumentRelevances> judgements)
      throws IOException {
    String line = null;
    BufferedReader reader = new BufferedReader(new FileReader(judgeFile));
    while ((line = reader.readLine()) != null) {
      // Line format: query \t docid \t grade
      Scanner s = new Scanner(line).useDelimiter("\t");
      String query = s.next();
      DocumentRelevances relevances = judgements.get(query);
      if (relevances == null) {
        relevances = new DocumentRelevances();
        judgements.put(query, relevances);
      }
      relevances.addDocument(Integer.parseInt(s.next()), s.next());
      s.close();
    }
    reader.close();
  }
  
  private static void writeToFileAll(String currentQuery, List<Integer> results,
      Map<String, DocumentRelevances> judgments) {
    String lineToWrite = currentQuery + "\t";
    lineToWrite += Double.toString(evaluatePrecision(currentQuery, results, judgments, 1)) + "\t";
    lineToWrite += Double.toString(evaluatePrecision(currentQuery, results, judgments, 5)) + "\t";
    lineToWrite += Double.toString(evaluatePrecision(currentQuery, results, judgments, 10)) + "\t";
    
    lineToWrite += Double.toString(evaluateRecall(currentQuery, results, judgments, 1)) + "\t";
    lineToWrite += Double.toString(evaluateRecall(currentQuery, results, judgments, 5)) + "\t";
    lineToWrite += Double.toString(evaluateRecall(currentQuery, results, judgments, 10)) + "\t";
    
    lineToWrite += Double.toString(evaluateFScore(currentQuery, results, judgments, 1, 0.5)) + "\t";
    lineToWrite += Double.toString(evaluateFScore(currentQuery, results, judgments, 5, 0.5)) + "\t";
    lineToWrite += Double.toString(evaluateFScore(currentQuery, results, judgments, 10, 0.5)) + "\t";
    
    lineToWrite += evaluatePrecisionAtRecallPoints(currentQuery, results, judgments) + "\t";
    
    lineToWrite += Double.toString(evaluateAveragePrecision(currentQuery, results, judgments)) + "\t";
    
    lineToWrite += Double.toString(evaluateNDCG(currentQuery, results, judgments, 1)) + "\t";
    lineToWrite += Double.toString(evaluateNDCG(currentQuery, results, judgments, 5)) + "\t";
    lineToWrite += Double.toString(evaluateNDCG(currentQuery, results, judgments, 10)) + "\t";
    
    lineToWrite += Double.toString(evaluateReciprocalRank(currentQuery, results, judgments));
    
    System.out.println(lineToWrite);
  }
  
  private static void switchBasedOnMetric(int metric, String currentQuery, List<Integer> results,
      Map<String, DocumentRelevances> judgments) {
    switch (metric) {
    case -1:
      evaluateQueryInstructor(currentQuery, results, judgments);
      break;
    case 0: System.out.println(currentQuery + "\t" + "Precison at 1" + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 1)));
            System.out.println(currentQuery + "\t" + "Precison at 5" + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 5)));
            System.out.println(currentQuery + "\t" + "Precison at 10" + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 10)));
            break;
            
    case 1: System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(1) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 1)));
            System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(5) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 5)));
            System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(10) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 10)));
            break;
    
    case 2: System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(1) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 1, 0.5)));
            System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(5) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 5, 0.5)));
            System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(10) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 10, 0.5)));
            break;
    
    
    case 3: System.out.println(currentQuery + "\t" + "Precision at Recall points:" + evaluatePrecisionAtRecallPoints(currentQuery, results, judgments));
            System.out.println();
            break;
    
    case 4: System.out.println(currentQuery + "\t" + "Average Precison"+  "\t" + Double.toString(evaluateAveragePrecision(currentQuery, results, judgments)));
            break;
    
    case 5: System.out.println(currentQuery + "\t" + "NDCG at 1"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 1)));
            System.out.println(currentQuery + "\t" + "NDCG at 5"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 5)));
            System.out.println(currentQuery + "\t" + "NDCG at 10"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 10)));
            break;
  
    case 6: System.out.println(currentQuery + "\t" + "Reciprocal rank " + "\t" + Double.toString(evaluateReciprocalRank(currentQuery, results, judgments)));
            break;
    
    
    case 7: writeToFileAll(currentQuery, results, judgments);
            break;
    
    default:
      // @CS2580: add your own metric evaluations above, using function
      // names like evaluateQueryMetric0.
      System.err.println("Requested metric not implemented!");
    
    }
  }

  // @CS2580: implement various metrics inside this function
  public static void evaluateStdin(
      int metric, Map<String, DocumentRelevances> judgments)
          throws IOException {
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    List<Integer> results = new ArrayList<Integer>();
    String line = null;
    String currentQuery = "";
    while ((line = reader.readLine()) != null) {
      // System.out.println(line);
      
    	Scanner s = new Scanner(line).useDelimiter("\t");
      final String query = s.next();
      if (!query.equals(currentQuery)) {
        //System.out.println(query + "    " + currentQuery);
        //System.out.println(Integer.toString(results.size()));
        if (results.size() > 0) {
          
          switchBasedOnMetric(metric, currentQuery, results, judgments);
          results.clear();
        }
        currentQuery = query;
      }
      results.add(Integer.parseInt(s.next()));
      /*try{
      	results.add(Integer.parseInt(s.next()));
      }
      catch(Exception e) {
      	System.out.println("Hello" + line);
      }*/
      
      s.close();
    }
    reader.close();
    if (results.size() > 0) {
        switchBasedOnMetric(metric, currentQuery, results, judgments);
//      evaluateQueryInstructor(currentQuery, results, judgments);
//      System.out.println(currentQuery + "\t" + "Precison at "+ Integer.toString(1) + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 1)));
//      System.out.println(currentQuery + "\t" + "Precison at "+ Integer.toString(5) + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 5)));
//      System.out.println(currentQuery + "\t" + "Precison at "+ Integer.toString(10) + "\t" + Double.toString(evaluatePrecision(currentQuery, results, judgments, 10)));
//      
//      System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(1) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 1)));
//      System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(5) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 5)));
//      System.out.println(currentQuery + "\t" + "Recall at "+ Integer.toString(10) + "\t" + Double.toString(evaluateRecall(currentQuery, results, judgments, 10)));
//      
//      
//      System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(1) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 1, 0.5)));
//      System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(5) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 5, 0.5)));
//      System.out.println(currentQuery + "\t" + "F SCore at "+ Integer.toString(10) + "\t" + Double.toString(evaluateFScore(currentQuery, results, judgments, 10, 0.5)));
//      
//      System.out.println(currentQuery + "\t" + "Reciprocal rank " + "\t" + Double.toString(evaluateReciprocalRank(currentQuery, results, judgments)));
//    
//      System.out.println(currentQuery + "\t" + "Average Precison"+  "\t" + Double.toString(evaluateAveragePrecision(currentQuery, results, judgments)));
//    
//      System.out.println(currentQuery + "\t" + "NDCG at 1"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 1)));
//      System.out.println(currentQuery + "\t" + "NDCG at 5"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 5)));
//      System.out.println(currentQuery + "\t" + "NDCG at 10"+  "\t" + Double.toString(evaluateNDCG(currentQuery, results, judgments, 10)));
//      
//      
//      evaluatePrecisionAtRecallPoints(currentQuery, results, judgments);
//      
    }
  }
  
  public static double evaluateFScore(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments, int K, double alpha) {
    double precision = 0;
    double recall = 0;
    double fScore = 0;
    DocumentRelevances relevances = judgments.get(query);
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    else {
      precision = evaluatePrecision(query, docids, judgments, K);
      recall = evaluateRecall(query, docids, judgments, K);
      double fScore_inv = alpha * (1/precision) + (1-alpha) * (1/recall);
      fScore = 1/fScore_inv;
    }
    return fScore; 
  }
  
  public static double evaluateReciprocalRank(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments) {
    double reciprocalRank = 0;
    DocumentRelevances relevances = judgments.get(query);
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    else {
      for(int i = 0; i < docids.size(); i++) {
        int docid = docids.get(i);
        if (relevances.hasRelevanceForDoc(docid)) {
          reciprocalRank =  (1/(i+1));
          return reciprocalRank;
        }
      }
    }
    return reciprocalRank;
  }
  
  public static double evaluateAveragePrecision(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments) {
    double RR = 0;
    double AP = 0;
    DocumentRelevances relevances = judgments.get(query);
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    else {
      for (int i = 0; i < docids.size(); i++) {
        int docid = docids.get(i); 
        if (relevances.hasRelevanceForDoc(docid) && relevances.getRelevanceForDoc(docid) == 1.0) {
          RR += 1;
          AP += (RR/(i+1));
        }
      }
    }
    if (RR == 0) {
      return 0;
    }
    return AP/RR;
  }
  
  public static String evaluatePrecisionAtRecallPoints(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments) {
  
    Map<Double, Double> PR = new HashMap<Double, Double>();
    Map<Double, Double> result = new HashMap<Double, Double>();
    //String resultLine = "{";
    String resultLine = "";
    //System.out.println("PR set:");
    
    for(int i = 1; i <= docids.size(); i++) {
      double precision = evaluatePrecision(query, docids, judgments, i);
      double recall = evaluateRecall(query, docids, judgments, i);
      if (!PR.containsKey(recall)) {
        PR.put(recall, precision);
        // System.out.println(Double.toString(recall) + " " + Double.toString(precision));
      }
      if(recall == 1.0) {
        break;
      }
    }
  
    //System.out.println("Results set:");
    
    Set<Double> keys = PR.keySet();
    for(int j = 0; j <= 10; j++){
      double i = j/10.0; 
      double max = 0;
      for(Double key: keys) {
        if(key >= i && PR.get(key) > max) {
          max = PR.get(key);
        }
      }
      result.put(i, max);
      resultLine += Double.toString(max) + "\t";
      //resultLine += ", "+ Double.toString(i) + " " + Double.toString(max);
      //System.out.println();
    }
    resultLine = resultLine.substring(0, resultLine.length() - 1);
    //resultLine += "}";
    //resultLine = resultLine.substring(1);
    return resultLine;
  }
  
  public static double evaluatePrecision(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments, int K) {
    double R = 0;
    DocumentRelevances relevances = judgments.get(query);
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    else {
      for (int i = 0; i < K; i++) {
        int docid = docids.get(i); 
        if (relevances.hasRelevanceForDoc(docid)) {
          R += relevances.getRelevanceForDoc(docid);
        }
      }
    }
    if (K == 0) {
      return 0;
    }
    return R/K;
  }
  
  public static double evaluateNDCG(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments, int K) {
    double DCG = 0;
    double IDCG = 0;
    DocumentRelevances relevances = judgments.get(query);
    
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    
    else {
      ArrayList<Double> grade_values = new ArrayList<Double>(relevances.relevancesWeight.values());
      Collections.sort(grade_values);
      Collections.reverse(grade_values);
      // System.out.println(grade_values);
      int range = Math.min(grade_values.size(), K);
      for(int i = 0; i < range; i++) {
        IDCG += grade_values.get(i)/(Math.log(i+2)/Math.log(2));
      }
      
      for (int i = 0; i < K; i++) {
        int docid = docids.get(i); 
        if (relevances.hasRelevanceForDoc(docid)) {
        	DCG += relevances.getRelevanceWeightForDoc(docid)/(Math.log(i+2)/Math.log(2));
        }
      }
    }
    // System.out.println(DCG);
    // System.out.println(IDCG);
    if (IDCG == 0) {
      return 0;
    }
    return DCG/IDCG;
  }
  
  
  public static double evaluateRecall(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments, int K) {
    double R = 0;
    double count = 0;
    DocumentRelevances relevances = judgments.get(query);
    if (relevances == null) {
      System.out.println("Query [" + query + "] not found!");
    }
    else {
      count = relevances.getTotalRelevantDocsCount();
      for (int i = 0; i < K; i++) {
        int docid = docids.get(i); 
        if (relevances.hasRelevanceForDoc(docid)) {
          R += relevances.getRelevanceForDoc(docid);
        }
      }
    }
    if (count == 0) {
      return 0;
    }
    return (R/count);
  }
  
  public static void evaluateQueryInstructor(
      String query, List<Integer> docids,
      Map<String, DocumentRelevances> judgments) {
    double R = 0.0;
    double N = 0.0;
    for (int docid : docids) {
      DocumentRelevances relevances = judgments.get(query);
      if (relevances == null) {
        System.out.println("Query [" + query + "] not found!");
      } else {
        if (relevances.hasRelevanceForDoc(docid)) {
          R += relevances.getRelevanceForDoc(docid);
        }
        ++N;
      }
    }
    System.out.println(query + "\t" + Double.toString(R / N));
  }
}
