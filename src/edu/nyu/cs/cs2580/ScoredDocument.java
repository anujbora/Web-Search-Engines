package edu.nyu.cs.cs2580;

/**
 * Document with score.
 * 
 * @author fdiaz
 * @author congyu
 */
class ScoredDocument implements Comparable<ScoredDocument> {
  private String _query;
  private Document _doc;
  private double _score;

  public ScoredDocument(String query, Document doc, double score) {
    _query = query;
    _doc = doc;
    _score = score;
  }

  public double get_score() {
	return _score;
  }

  public void set_score(double _score) {
	this._score = _score;
  }
  
  public Integer get_doc_id() {
  	return _doc._docid;
  }

  public String asTextResult() {
    StringBuffer buf = new StringBuffer();
    buf.append(_query).append("\t");
    buf.append(_doc._docid).append("\t");
    buf.append(_doc.getTitle()).append("\t");
    buf.append(_score);
    return buf.toString();
  }

  /**
   * @CS2580: Student should implement {@code asHtmlResult} for final project.
   */
  public String asHtmlResult() {
    return "<h2>" + _doc.getTitle() + "</h2>"
    		+ "<h6> Document ID = " + _doc._docid + "\t Number of Views = " + 
    		_doc.getNumViews();
  }

  @Override
  public int compareTo(ScoredDocument o) {
    if (this._score == o._score) {
      return 0;
    }
    return (this._score > o._score) ? 1 : -1;
  }
}
