package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Use this template to implement the cosine ranker for HW1.
 * 
 * @author congyu
 * @author fdiaz
 */
public class RankerCosine extends Ranker {

  public RankerCosine(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
    Vector<ScoredDocument> all = new Vector<ScoredDocument>();
	
    HashMap<String,Double> query_tf_idf = new HashMap<String,Double>();
    
    double normalizingSum = 0;
    int N = _indexer._numDocs;
    
    for(String token: query._tokens) {
    	
    	if(!query_tf_idf.containsKey(token)) {
    		
    		int tf = Collections.frequency(query._tokens, token);
    		int docFreq = _indexer.corpusDocFrequencyByTerm(token);
    		//here
    		double idf = 1;
    		if(docFreq != 0){
    			idf = 1 + (Math.log10((double)N / (double)_indexer.corpusDocFrequencyByTerm(token)) / Math.log10(2));
    		}
    		
    		double value = tf*idf;
    		query_tf_idf.put(token, value);
    		normalizingSum += (value*value); 
    	}
    }
    
    if( normalizingSum > 0){
    	
    	normalizingSum = Math.sqrt(normalizingSum);
    
    	for(String token: query_tf_idf.keySet()) {
    	
    		query_tf_idf.replace(token, (Double)query_tf_idf.get(token)/normalizingSum);
    	
    	}
    }
    
	for (int i = 0; i < _indexer.numDocs(); ++i) {
      all.add(scoreDocument(query, query_tf_idf, i));
    }
	    
	Collections.sort(all, Collections.reverseOrder());
	
	// Normalizing Scores
	double maxScore = all.get(0).get_score();
	double minScore = all.get(all.size()-1).get_score();
	
	//If no documents match the search term, use Numviews Ranker
	if(maxScore == 0 && minScore == 0) {
		RankerNumviews numviews = new RankerNumviews(_options, _arguments, _indexer);
		return numviews.runQuery(query, numResults);
	}
	
	double scoreRange = maxScore - minScore;

	Vector<ScoredDocument> results = new Vector<ScoredDocument>();
	for (int i = 0; i < Math.min(all.size(), numResults); i++) {
	  double score = (all.get(i).get_score() - minScore ) / scoreRange;
	  all.get(i).set_score(score);
	  results.add(all.get(i));
	}
	    
	return results;
  }
  
  private ScoredDocument scoreDocument(Query query, HashMap<String,Double> query_tf_idf, int did) {
		
	    DocumentFull doc = (DocumentFull) _indexer.getDoc(did);
	    
	    double score = 0;
	    
	    Vector<String> allTokens = doc.getConvertedBodyTokens();
	    
	    //Optimization start
	    boolean flag = true;
	    for (String q: query._tokens) {
	    	
	    	if(allTokens.contains(q)) {
	    		flag = false;
	    		break;
	    	}
	    }
	    
	    if(flag){
	    	return new ScoredDocument(query._query, doc, 0);
	    }
	  //Optimization end
	    
	    
	    HashMap<String,Double> tf_idf = new HashMap<String,Double>();
	    
	    for(String token: allTokens) {
	    	
	    	if(!tf_idf.containsKey(token)) {
	    		
	    		tf_idf.put(token, (double) 1);
	    	} else
	    	{
	    		tf_idf.replace(token, tf_idf.get(token) + 1);
	    	}
	    }
	    
	    
	    double normalizingSum = 0;
	    int N = _indexer._numDocs;
	    
	    for(String token: tf_idf.keySet()) {	    	
	    		
	    		Double tf = tf_idf.get(token);
	    		double idf = 1;
	    		int docFreq = _indexer.corpusDocFrequencyByTerm(token);
	    		if(docFreq != 0) {
	    			 idf = 1 + (Math.log10((double)N / (double)docFreq) / Math.log10(2));
	    		}
	    		double value = tf*idf;
	    		tf_idf.replace(token, value);
	    		normalizingSum += (value*value); 

	    }
	    
	    if  (normalizingSum > 0) {
	    	normalizingSum = Math.sqrt(normalizingSum);
	    
	    	for(String token: tf_idf.keySet()) {
	    	
	    		tf_idf.replace(token, (double)tf_idf.get(token)/normalizingSum);
	    	
	    	}
	    }
	    
	    for (String q: query._tokens) {
	    	
	    	if(tf_idf.containsKey(q)) {
	    		score += tf_idf.get(q) * query_tf_idf.get(q);
	    	}
	    }
	       
	    return new ScoredDocument(query._query, doc, score);
  }
}
